#ifndef __ONT_WORKFLOW_HPP__
#define __ONT_WORKFLOW_HPP__

#include <memory>
#include <set>
#include <stdexcept>

#include "ont_task.hpp"

namespace ont
{

class workflow_error: public std::runtime_error
{
public:
    workflow_error(const char* message);
};


/// A workflow represents tasks that need to be performed in a certain order.
/// Some tasks (aka. workflow steps) can be performed in parallel, others might
/// strictly depend on another step. The workflow is 'acyclic'.
class workflow
{
public:
    /// Constructs a workflow object.
    workflow();

    /// Destructs a workflow object.
    ~workflow();

    /// Adds a new step to the workflow.
    /// @param step The task to add to the workflow.
    /// @param depends_on Another tasks that the step depends on and which
    ///     needs to be performed earlier. Optional.
    void add_step(task* step, task* depends_on = nullptr);

    /// Adds a new step to the workflow.
    /// @param step The task to add to the workflow.
    /// @param depends_on Other tasks that the step depends on and which
    ///     need to be performed earlier.
    void add_step(task* step, const std::set<task*>& depends_on);

    /// Returns the steps that do not depend on any other step.
    /// @return The steps that do not depend on any other step.
    std::set<task*> start_steps() const;

    /// Returns the steps that the given step depends on.
    /// @return The steps that the given step depends on.
    std::set<task*> previous_steps(const task* step) const;

    /// Returns the steps that depend on the given step.
    /// @return The steps that depend on the given step.
    std::set<task*> next_steps(const task* step) const;

    class cursor;

    /// A cursor that points at the initial state of execution of the workflow.
    /// @return A new cursor that points at the beginning of traversing through
    ///     the workflow graph.
    cursor begin();

    /// A cursor that points at the final, finished state of execution of the
    /// workflow.
    /// @return A dummy cursor that shows that no steps are left.
    cursor end();

    /// Processes the workflow steps.
    /// Automatic steps are performed in parallel. Only one manual step can be
    /// performed at a time, but automatic tasks can be performed while a
    /// manual step is performed by the user.
    void process();

private:
    struct impl;
    std::unique_ptr<impl> d;
};


/// A workflow cursor represents the progress of the execution of a workflow.
class workflow::cursor
{
    friend class workflow;

    /// Constructs a workflow cursor object.
    /// @param wf The workflow that this cursor belongs to.
    /// @param end Tells if the cursor should point at the end or the beginning
    ///     of the workflow.
    cursor(workflow* wf, bool end = false);

public:

    /// Move constructs a workflow cursor object.
    cursor(cursor&& other);

    /// Destructs a workflow cursor object.
    ~cursor();

    /// The set of steps that represents the current status of the execution of
    /// the workflow.
    /// @return The set of tasks that are being or can be executed now.
    std::set<task*> current_steps() const;

    /// It marks the given workflow step as 'done' and updates the status of
    /// the cursor to represent the progress. The given step is removed from
    /// the current steps. The steps that depend on the given step will be
    /// added to the current steps, assuming that all their other dependencies
    /// have already been 'done' as well, if any.
    /// @param step The workflow step to be marked as 'done'.
    /// @return The new steps added to the set of current steps, if any.
    std::set<task*> mark_done(task* step);

    /// Checks if the current object equals the given other object.
    /// @param other The other object to compare the current object with.
    /// @return bool `true` if the objects are equal, otherwise false.
    bool operator==(const cursor& other) const;

    /// Checks if the current object differs from the given other object.
    /// @param other The other object to compare the current object with.
    /// @return bool `true` if the objects are not equal, otherwise false.
    bool operator!=(const cursor& other) const;

private:
    struct impl;
    std::unique_ptr<impl> d;
};

}

#endif
