#ifndef __ONT_TASK_HPP__
#define __ONT_TASK_HPP__

#include <string>

namespace ont
{

/// Represents a task that can be part or a workflow.
class task
{
public:
    /// Constructs an automatic task object.
    task();

    /// Tells if the task is automatic or not.
    /// @return True if the task is automatic, otherwise false.
    bool is_automatic() const;

    /// Tells if the task is manual or not.
    /// @return True if the task is manual, otherwise false.
    bool is_manual() const;

    /// Performs the task. It is meant to be overridden.
    /// The current implementation is empty.
    virtual void perform();

protected:
    /// Constructs a workflow task object.
    /// @param automatic Specifies if the task is automatic or manual.
    task(bool automatic);

private:
    bool automatic;
};


class automatic_task: public task
{
};


/// Represents a manual task.
class manual_task: public task
{
public:
    /// Constructs a workflow task object.
    /// @param instructions The instructions for the user to perform the task.
    manual_task(const std::string& instructions);

    /// It prints out the instructions to the standard output for performing the
    /// task. Then it waits for the user's confirmation from the standard input
    /// that the task has been performed.
    virtual void perform() override;

private:
    std::string instructions;
};

}

#endif
