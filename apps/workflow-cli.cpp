#include <iostream>

#include <ont_workflow.hpp>

#include <chrono>
#include <string>
#include <thread>


using namespace std::chrono_literals;


/// Dummy automated task that waits for a certain period of time.
struct timed_automatic_task: ont::automatic_task
{
    timed_automatic_task(const std::string& name, std::chrono::seconds length)
        : name(name),
          length(length)
    {
    }

    virtual void perform() override
    {
        std::cout << name << " started.\n";
        std::this_thread::sleep_for(length);
        std::cout << name << " finished.\n";
    }

private:
    std::string name;
    std::chrono::seconds length;
};


int main()
{
    std::cout <<
        "\n"
        "               Workflow Challenge\n"
        "\n"
        "The workflow consists of the following steps:\n"
        "\n"
        " - Adding the reagents will need to be done manually.\n"
        " - Preheating the heater is automatic and it will take 10 seconds.\n"
        " - Mixing the reagents is also automatic, it will take 15 seconds\n"
        "   and it will be done while the heater is being preheated.\n"
        " - Heating the samples is also automatic and it will take 5 seconds.\n"
        " - Extracting the samples will need to be done manually.\n"
        "\n"
        "Ready to start?\n"
        "\n";

    std::string answer;
    std::cin >> answer;

    ont::manual_task add_reagent_1("Add the first reagent.");
    ont::manual_task add_reagent_2("Add the other reagent.");
    timed_automatic_task preheat_heater("Preheating heater", 10s);
    timed_automatic_task mix_reagents("Mixing reagents", 15s);
    timed_automatic_task heat_sample("Heating sample", 5s);
    ont::manual_task extract_sample("Extract the sample.");

    ont::workflow w;
    w.add_step(&add_reagent_1);
    w.add_step(&add_reagent_2, &add_reagent_1);
    w.add_step(&preheat_heater, &add_reagent_2);
    w.add_step(&mix_reagents, &add_reagent_2);
    w.add_step(&heat_sample, {&preheat_heater, &mix_reagents});
    w.add_step(&extract_sample, &heat_sample);

    w.process();

    std::cout << "\nWell done!\n\n";
}
