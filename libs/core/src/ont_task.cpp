#include <ont_task.hpp>

#include <iostream>
#include <regex>

namespace ont
{

task::task()
    : automatic(true)
{
}


task::task(bool automatic)
    : automatic(automatic)
{
}


bool task::is_automatic() const
{
    return automatic;
}


bool task::is_manual() const
{
    return !automatic;
}


void task::perform()
{
}


manual_task::manual_task(const std::string& instructions)
    : task(false),
      instructions(instructions)
{
}


void manual_task::perform()
{
    std::cout << instructions;

    std::string answer;
    do
    {
        std::cout << "\nPlease type 'yes' when the task has been performed: ";
        std::cout.flush();
        std::cin >> answer;
    }
    while (answer != "y" && answer != "yes");
}

}
