#include <ont_workflow.hpp>

#include <algorithm>
#include <future>
#include <map>
#include <mutex>
#include <queue>
#include <vector>

namespace ont
{

workflow_error::workflow_error(const char* message)
    : std::runtime_error(message)
{
}


struct workflow::impl
{
    /// Dependency relation between workflow steps.
    /// The keys are tasks, the values are other tasks that they depend on.
    std::map<const task*, std::set<task*>> previous_steps;

    /// Steps that the workflow starts with.
    /// Redundant information. It should always be equal to the set of keys
    /// in `previous_steps` whose value is an empty set.
    std::set<task*> start_steps;

    /// Steps that the workflow may continue with.
    /// Redundant information. Stored for convenient access.
    std::map<const task*, std::set<task*>> next_steps;
};


workflow::workflow()
    : d{std::make_unique<impl>()}
{
}


workflow::~workflow() = default;


void workflow::add_step(task* step, task* previous_step)
{
    if (d->previous_steps.find(step) != d->previous_steps.end())
    {
        throw workflow_error("Step already added to the workflow.");
    }

    if (!previous_step)
    {
        d->previous_steps[step];
        d->start_steps.insert(step);
        return;
    }

    if (d->previous_steps.find(previous_step) == d->previous_steps.end())
    {
        throw workflow_error("Previous step has not been added to the workflow.");
    }

    d->previous_steps[step].insert(previous_step);
    d->next_steps[previous_step].insert(step);
}


void workflow::add_step(task* step, const std::set<task*>& previous_steps)
{
    if (d->previous_steps.find(step) != d->previous_steps.end())
    {
        throw workflow_error("Step already added to the workflow.");
    }

    if (previous_steps.empty())
    {
        d->previous_steps[step];
        d->start_steps.insert(step);
        return;
    }

    for (auto& previous_step: previous_steps)
    {
        if (d->previous_steps.find(previous_step) == d->previous_steps.end())
        {
            throw workflow_error("Previous step has not been added to the workflow.");
        }

        d->previous_steps[step].insert(previous_step);
        d->next_steps[previous_step].insert(step);
    }
}


std::set<task*> workflow::start_steps() const
{
    return d->start_steps;
}


std::set<task*> workflow::previous_steps(const task* step) const
{
    auto it = d->previous_steps.find(step);
    return it != d->previous_steps.end() ? it->second : std::set<task*>{};
}


std::set<task*> workflow::next_steps(const task* step) const
{
    auto it = d->next_steps.find(step);
    return it != d->next_steps.end() ? it->second : std::set<task*>{};
}


void workflow::process()
{
    std::mutex mx;
    std::unique_lock<std::mutex> lk(mx, std::defer_lock);
    std::condition_variable cv;

    std::mutex mx_manual;
    std::condition_variable cv_manual;
    bool manual_in_progress{};

    auto it = begin();
    auto it_end = end();

    if (it == it_end)
    {
        return;
    }

    std::queue<task*> q;
    auto push_each = [&q](const std::set<task*>& steps) {
        std::for_each(steps.begin(), steps.end(), [&q](task* t){ q.push(t); });
    };
    push_each(start_steps());

    auto next_step = q.front();
    q.pop();

    /// Note:
    /// The async call returns a future and the destructor of the future blocks
    /// the owning thread until the concurrent ('asynced') thread finishes.
    /// To prevent the early destruction (and blocking), the futures need to be
    /// stored outside of the loop.
    std::vector<std::future<void>> futures;

    while (true)
    {
        if (next_step->is_automatic())
        {
            futures.push_back(std::async(
                [next_step, &it, &push_each, &mx, &cv, &futures]
                {
                    next_step->perform();
                    {
                        std::lock_guard<std::mutex> lock(mx);
                        push_each(it.mark_done(next_step));
                    }
                    cv.notify_all();
                }
            ));
        }
        else
        {
            futures.push_back(std::async(
                [next_step, &it, &push_each, &mx, &cv, &futures,
                        &mx_manual, &cv_manual, &manual_in_progress]
                {
                    {
                        std::unique_lock<std::mutex> lock(mx_manual);
                        cv_manual.wait(lock, [&manual_in_progress]{ return !manual_in_progress; });
                        manual_in_progress = true;
                        next_step->perform();
                        manual_in_progress = false;
                    }
                    cv_manual.notify_all();

                    {
                        std::lock_guard<std::mutex> lock(mx);
                        push_each(it.mark_done(next_step));
                    }
                    cv.notify_all();
                }
            ));
        }

        lk.lock();
        cv.wait(lk, [&q, &it, &it_end]{ return !q.empty() || it == it_end; });
        if (q.empty() && it == it_end)
        {
            return;
        }
        next_step = q.front();
        q.pop();
        lk.unlock();
    }
}


workflow::cursor workflow::begin()
{
    return cursor(this);
}


workflow::cursor workflow::end()
{
    return cursor(this, true);
}


struct workflow::cursor::impl
{
    impl(const workflow* wf, bool end);

    const workflow* wf;
    std::set<task*> current_steps;
    std::map<task*, std::size_t> num_of_deps;
};


workflow::cursor::impl::impl(const workflow* wf, bool end)
    : wf(wf),
      current_steps(end ? std::set<task*>{} : wf->d->start_steps)
{
}


workflow::cursor::cursor(workflow* wf, bool end)
    : d{std::make_unique<impl>(wf, end)}
{
}


workflow::cursor::cursor(workflow::cursor&& other) = default;


workflow::cursor::~cursor() = default;


std::set<task*> workflow::cursor::current_steps() const
{
    return d->current_steps;
}


std::set<task*> workflow::cursor::mark_done(task* current_step)
{
    auto it_current_step = d->current_steps.find(current_step);
    if (it_current_step == d->current_steps.end())
    {
        throw workflow_error("The given step is not being executed.");
    }

    std::set<task*> new_steps;

    for (auto& next_step: d->wf->next_steps(*it_current_step))
    {
        auto it_num_of_deps = d->num_of_deps.find(next_step);
        if (it_num_of_deps == d->num_of_deps.end())
        {
            auto num_of_deps = d->wf->d->previous_steps[next_step].size() - 1;
            if (num_of_deps > 0)
            {
                d->num_of_deps[next_step] = num_of_deps;
            }
            else
            {
                new_steps.insert(next_step);
            }
        }
        else if (--it_num_of_deps->second == 0)
        {
            d->num_of_deps.erase(it_num_of_deps);
            new_steps.insert(next_step);
        }
    }

    d->current_steps.erase(it_current_step);
    d->current_steps.insert(new_steps.begin(), new_steps.end());

    return new_steps;
}


bool workflow::cursor::operator==(const cursor& other) const
{
    return d->wf == other.d->wf
            && d->current_steps == other.d->current_steps
            && d->num_of_deps == other.d->num_of_deps;
}


bool workflow::cursor::operator!=(const cursor& other) const
{
    return !(*this == other);
}

}
