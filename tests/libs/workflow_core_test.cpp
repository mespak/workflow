#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <ont_workflow.hpp>

#include <chrono>
#include <thread>

using tasks_t = std::set<ont::task*>;

TEST_CASE("Creating a workflow")
{
    ont::workflow w;

    ont::task step1;
    ont::task step2;
    ont::task step3;
    ont::task step4;

    CHECK_EQ(w.start_steps(), tasks_t{});
    CHECK_EQ(w.previous_steps(&step1), tasks_t{});
    CHECK_EQ(w.next_steps(&step1), tasks_t{});

    w.add_step(&step1);
    w.add_step(&step2, &step1);
    w.add_step(&step3, &step1);
    w.add_step(&step4, {&step2, &step3});

    CHECK_EQ(w.start_steps(), tasks_t{&step1});

    CHECK_EQ(w.previous_steps(&step1), tasks_t{});
    CHECK_EQ(w.previous_steps(&step2), tasks_t{&step1});
    CHECK_EQ(w.previous_steps(&step3), tasks_t{&step1});
    CHECK_EQ(w.previous_steps(&step4), tasks_t{&step2, &step3});

    CHECK_EQ(w.next_steps(&step1), tasks_t{&step2, &step3});
    CHECK_EQ(w.next_steps(&step2), tasks_t{&step4});
    CHECK_EQ(w.next_steps(&step3), tasks_t{&step4});
    CHECK_EQ(w.next_steps(&step4), tasks_t{});

    CHECK_THROWS_WITH_AS(w.add_step(&step1, &step1),
                         "Step already added to the workflow.",
                         ont::workflow_error);
    CHECK_THROWS_WITH_AS(w.add_step(&step1, &step2),
                         "Step already added to the workflow.",
                         ont::workflow_error);
    CHECK_THROWS_WITH_AS(w.add_step(&step2, &step1),
                         "Step already added to the workflow.",
                         ont::workflow_error);

    ont::task step5;
    ont::task step6;

    CHECK_THROWS_WITH_AS(w.add_step(&step5, &step5),
                         "Previous step has not been added to the workflow.",
                         ont::workflow_error);
    CHECK_THROWS_WITH_AS(w.add_step(&step5, &step6),
                         "Previous step has not been added to the workflow.",
                         ont::workflow_error);
}


TEST_CASE("Workflow cursor")
{
    ont::task step1;
    ont::task step2;
    ont::task step3;
    ont::task step4;

    ont::workflow w;
    w.add_step(&step1);
    w.add_step(&step2, &step1);
    w.add_step(&step3, &step1);
    w.add_step(&step4, {&step2, &step3});

    SUBCASE("Order: 1 2 3 4")
    {
        auto c = w.begin();
        tasks_t new_steps;

        CHECK_EQ(c.current_steps(), w.start_steps());
        CHECK_NE(c, w.end());

        CHECK_THROWS_WITH_AS(c.mark_done(&step2),
                             "The given step is not being executed.",
                             ont::workflow_error);

        CHECK_EQ(c.current_steps(), w.start_steps());
        CHECK_NE(c, w.end());

        new_steps = c.mark_done(&step1);
        CHECK_EQ(c.current_steps(), tasks_t{&step2, &step3});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{&step2, &step3});

        CHECK_THROWS_WITH_AS(c.mark_done(&step4),
                             "The given step is not being executed.",
                             ont::workflow_error);
        CHECK_NE(c, w.end());

        new_steps = c.mark_done(&step2);
        CHECK_EQ(c.current_steps(), tasks_t{&step3});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{});

        new_steps = c.mark_done(&step3);
        CHECK_EQ(c.current_steps(), tasks_t{&step4});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{&step4});

        new_steps = c.mark_done(&step4);
        CHECK_EQ(c.current_steps(), tasks_t{});
        CHECK_EQ(c, w.end());
        CHECK_EQ(new_steps, tasks_t{});
    }

    SUBCASE("Order: 1 3 2 4")
    {
        auto c = w.begin();
        tasks_t new_steps;

        CHECK_EQ(c.current_steps(), w.start_steps());
        CHECK_NE(c, w.end());

        CHECK_THROWS_WITH_AS(c.mark_done(&step2),
                             "The given step is not being executed.",
                             ont::workflow_error);

        CHECK_EQ(c.current_steps(), w.start_steps());
        CHECK_NE(c, w.end());

        new_steps = c.mark_done(&step1);
        CHECK_EQ(c.current_steps(), tasks_t{&step2, &step3});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{&step2, &step3});

        CHECK_THROWS_WITH_AS(c.mark_done(&step4),
                             "The given step is not being executed.",
                             ont::workflow_error);
        CHECK_NE(c, w.end());

        new_steps = c.mark_done(&step3);
        CHECK_EQ(c.current_steps(), tasks_t{&step2});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{});

        new_steps = c.mark_done(&step2);
        CHECK_EQ(c.current_steps(), tasks_t{&step4});
        CHECK_NE(c, w.end());
        CHECK_EQ(new_steps, tasks_t{&step4});

        new_steps = c.mark_done(&step4);
        CHECK_EQ(c.current_steps(), tasks_t{});
        CHECK_EQ(c, w.end());
        CHECK_EQ(new_steps, tasks_t{});
    }

    SUBCASE("Multiple iterators")
    {
        auto c1 = w.begin();
        auto c2 = w.begin();
        auto c3 = w.begin();
        auto c4 = w.end();
        auto c5 = w.end();

        CHECK_EQ(c1, c2);
        CHECK_EQ(c1, c3);
        CHECK_EQ(c2, c3);
        CHECK_NE(c4, c1);
        CHECK_NE(c4, c2);
        CHECK_EQ(c4, c5);

        ont::workflow w2;
        w2.add_step(&step1);
        w2.add_step(&step2, &step1);
        w2.add_step(&step3, &step1);
        w2.add_step(&step4, {&step2, &step3});

        CHECK_NE(w.begin(), w2.begin());
        CHECK_NE(w.end(), w2.end());
    }
}


TEST_CASE("Workflow tasks automatic or manual")
{
    ont::task step1;
    ont::automatic_task step2;
    ont::manual_task step3("Mix everything.");

    CHECK(step1.is_automatic());
    CHECK(!step1.is_manual());

    CHECK(step2.is_automatic());
    CHECK(!step2.is_manual());

    CHECK(!step3.is_automatic());
    CHECK(step3.is_manual());
}


TEST_CASE("Workflow process")
{
    using namespace std::chrono_literals;
    using namespace ont;

    struct timed_task: task
    {
        timed_task(const std::string& name, std::chrono::milliseconds time_length)
            : name(name),
              time_length(time_length)
        {
        }
        virtual void perform() override
        {
            INFO(name, " started.");
            std::this_thread::sleep_for(time_length);
            INFO(name, " finished.");
        }
    private:
        std::string name;
        std::chrono::milliseconds time_length;
    };

    SUBCASE("Parallel execution shallow")
    {
        timed_task step1("Step 1", 10ms);
        timed_task step2("Step 2", 40ms);
        timed_task step3("Step 3", 40ms);
        timed_task step4("Step 4", 40ms);
        timed_task step5("Step 5", 40ms);
        timed_task step6("Step 6", 40ms);
        timed_task step7("Step 7", 40ms);
        timed_task step8("Step 8", 10ms);

        workflow w;
        w.add_step(&step1);
        w.add_step(&step2, &step1);
        w.add_step(&step3, &step1);
        w.add_step(&step4, &step1);
        w.add_step(&step5, &step1);
        w.add_step(&step6, &step1);
        w.add_step(&step7, &step1);
        w.add_step(&step8, {&step2, &step3, &step4, &step5, &step6, &step7});

        auto start_time = std::chrono::steady_clock::now();
        w.process();
        auto finish_time = std::chrono::steady_clock::now();

        auto process_duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - start_time);
        INFO("Process duration: ", process_duration.count());
        CHECK_LT(process_duration, 80ms);
    }

    SUBCASE("Parallel execution deep")
    {
        timed_task step1("Step 1", 10ms);
        timed_task step2("Step 2", 10ms);
        timed_task step3("Step 3", 10ms);
        timed_task step4("Step 4", 10ms);
        timed_task step5("Step 5", 10ms);
        timed_task step6("Step 6", 40ms);
        timed_task step7("Step 7", 10ms);

        workflow w;
        w.add_step(&step1);
        w.add_step(&step2, &step1);
        w.add_step(&step3, &step2);
        w.add_step(&step4, &step3);
        w.add_step(&step5, &step4);
        w.add_step(&step6, &step1);
        w.add_step(&step7, {&step5, &step6});

        auto start_time = std::chrono::steady_clock::now();
        w.process();
        auto finish_time = std::chrono::steady_clock::now();

        auto process_duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - start_time);
        INFO("Process duration: ", process_duration.count());
        CHECK_LT(process_duration, 80ms);
    }

    struct timed_manual_task: manual_task
    {
        timed_manual_task(const std::string& name, std::chrono::milliseconds time_length)
            : manual_task("Mix everything"),
              name(name),
              time_length(time_length)
        {
        }
        virtual void perform() override
        {
            INFO(name, " started.");
            std::this_thread::sleep_for(time_length);
            INFO(name, " finished.");
        }
    private:
        std::string name;
        std::chrono::milliseconds time_length;
    };

    SUBCASE("Parallel execution manual shallow")
    {
        timed_task step1("Step 1", 10ms);
        timed_manual_task step2("Step 2", 10ms);
        timed_manual_task step3("Step 3", 10ms);
        timed_manual_task step4("Step 4", 10ms);
        timed_manual_task step5("Step 5", 10ms);
        timed_manual_task step6("Step 6", 10ms);
        timed_task step7("Step 7", 10ms);

        workflow w;
        w.add_step(&step1);
        w.add_step(&step2, &step1);
        w.add_step(&step3, &step1);
        w.add_step(&step4, &step1);
        w.add_step(&step5, &step1);
        w.add_step(&step6, &step1);
        w.add_step(&step7, {&step2, &step3, &step4, &step5, &step6});

        auto start_time = std::chrono::steady_clock::now();
        w.process();
        auto finish_time = std::chrono::steady_clock::now();

        auto process_duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - start_time);
        INFO("Process duration: ", process_duration.count());
        CHECK_GT(process_duration, 70ms);
        CHECK_LT(process_duration, 90ms);
    }

    SUBCASE("Parallel execution manual deep")
    {
        timed_task step1("Step 1", 10ms);
        timed_task step2("Step 2", 10ms);
        timed_task step3("Step 3", 10ms);
        timed_task step4("Step 4", 10ms);
        timed_task step5("Step 5", 10ms);
        timed_manual_task step6("Step 6", 40ms);
        timed_task step7("Step 7", 10ms);

        workflow w;
        w.add_step(&step1);
        w.add_step(&step2, &step1);
        w.add_step(&step3, &step2);
        w.add_step(&step4, &step3);
        w.add_step(&step5, &step4);
        w.add_step(&step6, &step1);
        w.add_step(&step7, {&step5, &step6});

        auto start_time = std::chrono::steady_clock::now();
        w.process();
        auto finish_time = std::chrono::steady_clock::now();

        auto process_duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(finish_time - start_time);
        INFO("Process duration: ", process_duration.count());
        CHECK_LT(process_duration, 80ms);
    }
}
