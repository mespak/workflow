## Workflow Challenge

Building the project requires CMake 3.10.0 or later and a C++14 compatible
compiler.

The instructions below are for 3.20.0 or later. The commands for earlier CMake
versions can be found in the Dockerfiles.

Building the project:

```
cmake -S . -B build
cmake --build build
```

Testing the project:

```
ctest --test-dir build
```

Running the sample application:

```
./build/apps/workflow-cli
```

Installing the project:

```
cmake --install build
```

Running the installed application:

```
workflow-cli
```
